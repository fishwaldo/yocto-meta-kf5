# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/5.108/portingAids/kdesignerplugin-5.108.0.tar.xz"
SRC_URI[sha256sum] = "4bf9b9072aa1bd823accadfeb09409516da001d2383c55c43a94abe89f0e6d45"

