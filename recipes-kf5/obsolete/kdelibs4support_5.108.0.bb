# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/5.108/portingAids/kdelibs4support-5.108.0.tar.xz"
SRC_URI[sha256sum] = "6e6244b9699e52f51ca072a5264bd11b649e8d6aee382f148a947d13896517df"

