# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/5.108/portingAids/kjsembed-5.108.0.tar.xz"
SRC_URI[sha256sum] = "2aadb47d31ba690c0a25bfc8705682c4911f1ae294a1ea147dd20f785d4dd1d9"

