# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/5.108/portingAids/kjs-5.108.0.tar.xz"
SRC_URI[sha256sum] = "f03921392bb54ba011e3275fb52c9ec8f975e49222639644aefecd9665809177"

