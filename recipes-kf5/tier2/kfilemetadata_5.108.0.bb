# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/5.108/kfilemetadata-5.108.0.tar.xz"
SRC_URI[sha256sum] = "fc5c633dc2c25e9d3b978f11a548bdc62630123e9a1260d61871b9cbcd451842"

