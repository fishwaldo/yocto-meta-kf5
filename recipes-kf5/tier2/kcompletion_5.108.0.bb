# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/5.108/kcompletion-5.108.0.tar.xz"
SRC_URI[sha256sum] = "b8bd3ba590b58408c1aeb6163e8ec751fe3695f1bcc4b8ce92e272b31c18ff39"

