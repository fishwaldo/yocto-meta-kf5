# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/5.108/knotifications-5.108.0.tar.xz"
SRC_URI[sha256sum] = "2659ae0d9922331de156626edb2d69f2e9eff8407b83e939fcc20cd9a5ac0d17"

