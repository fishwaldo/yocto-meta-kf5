# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/5.108/kjobwidgets-5.108.0.tar.xz"
SRC_URI[sha256sum] = "98e907daf537d9435b2597179bc5ea384519187aa47b46d586e3608cfa4b1b6e"

