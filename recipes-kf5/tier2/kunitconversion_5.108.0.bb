# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/5.108/kunitconversion-5.108.0.tar.xz"
SRC_URI[sha256sum] = "72e05cd5b3fa677967c449e53a19a3eecf863b335ff65716af42140d3a2f9fcf"

