# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/5.108/kpackage-5.108.0.tar.xz"
SRC_URI[sha256sum] = "2ff04db7abd0b2adea8cda84deca62087f0a0a63eedbd84074c62a44622f28a0"

