# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/5.108/syndication-5.108.0.tar.xz"
SRC_URI[sha256sum] = "10b99641aa8e028bec25be8a1f4b95fa6f38b5b6785ee531a54dc8cee3873e60"

