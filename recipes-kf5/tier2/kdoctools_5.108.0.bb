# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/5.108/kdoctools-5.108.0.tar.xz"
SRC_URI[sha256sum] = "997f4b7c7fd25a18c45833becef12efbf0fe67df91249860d257122b87da237e"

