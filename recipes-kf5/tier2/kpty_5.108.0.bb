# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/5.108/kpty-5.108.0.tar.xz"
SRC_URI[sha256sum] = "fc3558eec885c0b66aec89acf5c324a46036bdfe24d1e3cb5f9d7ec5c5966186"

