# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/5.108/kcrash-5.108.0.tar.xz"
SRC_URI[sha256sum] = "3ec58b0aaad92aeb6cf84b509c308151daeb0ed856fc9bf0baabb2aeb5f320a5"

