# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/5.108/kauth-5.108.0.tar.xz"
SRC_URI[sha256sum] = "5f6e5133737b5f957361530d63080a58b93571d289b36f0a52616bf973d8c076"

