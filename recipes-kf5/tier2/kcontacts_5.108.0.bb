# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/5.108/kcontacts-5.108.0.tar.xz"
SRC_URI[sha256sum] = "a449832f176e684a675bdd0fcf4adf56de4f26892b84d86eaf76e8670b70a697"

