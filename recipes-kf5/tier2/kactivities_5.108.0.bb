# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/5.108/kactivities-5.108.0.tar.xz"
SRC_URI[sha256sum] = "901ee63f7e7ec421bf6ccb8cf1bfabca27421df2338cdccd1909f29068751053"

