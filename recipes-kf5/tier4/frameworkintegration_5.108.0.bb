# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/5.108/frameworkintegration-5.108.0.tar.xz"
SRC_URI[sha256sum] = "41b8d78fd45c98417729503da1b43607496bc63df091ca6f131ac3d8cd51eb27"

