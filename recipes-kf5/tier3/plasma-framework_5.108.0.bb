# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/5.108/plasma-framework-5.108.0.tar.xz"
SRC_URI[sha256sum] = "6b199da9db26d9ce7646bed253abc5c52b56bfe0ae5a03c0bc8bd4e8abea3f8c"

