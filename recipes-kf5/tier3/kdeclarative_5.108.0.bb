# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/5.108/kdeclarative-5.108.0.tar.xz"
SRC_URI[sha256sum] = "9ff83457840c31b7dc2ab8345f764696db53e4b21699996737f4dd2e140aafcd"

