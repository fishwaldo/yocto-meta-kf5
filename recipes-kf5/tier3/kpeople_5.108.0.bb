# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/5.108/kpeople-5.108.0.tar.xz"
SRC_URI[sha256sum] = "93384b7b4f973fbf1f3126d570379e595d8e4b8a9df0302459d2e602aeb7524c"

