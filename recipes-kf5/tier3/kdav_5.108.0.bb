# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/5.108/kdav-5.108.0.tar.xz"
SRC_URI[sha256sum] = "396e636c168e7283aeb1e31e24f16c02abc5a1e0570c3c95bfa102e5daf7d74e"

