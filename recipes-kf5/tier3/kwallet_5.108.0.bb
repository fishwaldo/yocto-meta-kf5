# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/5.108/kwallet-5.108.0.tar.xz"
SRC_URI[sha256sum] = "5897063187a97e8d1eccc3ac8b516d17bac9b74963d4526b90320e591104a8ff"

