# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/5.108/kconfigwidgets-5.108.0.tar.xz"
SRC_URI[sha256sum] = "86dcf6fd569696e8bb09a155dfae73d96d087cf8d5c1fd3967bb0195fb0a5fe5"

