# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/5.108/knotifyconfig-5.108.0.tar.xz"
SRC_URI[sha256sum] = "8a796fb8118391598467d385d27ad5767912f659ca4cff6c9251b38899377eb5"

