# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/5.108/kio-5.108.0.tar.xz"
SRC_URI[sha256sum] = "42044674b206c79bf8c9a40a91c7eaf754fff473d7b13fdb6efc160d92bcabec"

