# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/5.108/purpose-5.108.0.tar.xz"
SRC_URI[sha256sum] = "0d83b4344f53bc0c3f774ccbfb43605fb0640d810d01bd83e9e09be4986fef3f"

