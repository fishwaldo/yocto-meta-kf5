# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/5.108/ktextwidgets-5.108.0.tar.xz"
SRC_URI[sha256sum] = "4c6c27445863b4ed4dd8121911db9b5ace872a14d51301498759759cb357e1e3"

