# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/5.108/krunner-5.108.0.tar.xz"
SRC_URI[sha256sum] = "309665a7868a84f7e5563a7b171e96e9ccae18c92ce78f09d5fccb3018085579"

