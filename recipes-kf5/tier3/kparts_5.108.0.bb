# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/5.108/kparts-5.108.0.tar.xz"
SRC_URI[sha256sum] = "ab110d0d46edfb6db9cf3ff4c91c123279c1ad92d605f3a2f278e2769bc09339"

