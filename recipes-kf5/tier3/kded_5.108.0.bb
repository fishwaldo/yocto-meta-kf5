# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/5.108/kded-5.108.0.tar.xz"
SRC_URI[sha256sum] = "4183da6b4e345e8ab93e6642cfb6f75b418ee6f9a2e10f9524b282fce51e4a21"

