# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/5.108/baloo-5.108.0.tar.xz"
SRC_URI[sha256sum] = "032129dbd9aba7b624fef8796c406fd797c98d93b0cbb6ac5a80ee4232b4c5d8"

