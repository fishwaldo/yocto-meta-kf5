# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/5.108/kactivities-stats-5.108.0.tar.xz"
SRC_URI[sha256sum] = "de9a89363a7a8478364761a5a3ec936343716fe0c032b8cf29827ec19f55770f"

