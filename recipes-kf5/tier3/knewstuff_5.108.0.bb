# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/5.108/knewstuff-5.108.0.tar.xz"
SRC_URI[sha256sum] = "5a9862a64886b824f7d4cf6287fcf3b7e079d5ec686e91a8a565b8e5999a9fc2"

