# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/5.108/portingAids/kxmlrpcclient-5.108.0.tar.xz"
SRC_URI[sha256sum] = "605fbd72578b57ce30e1c882af27fc736a9e5d0046b07a93a38dd7a06461c55d"

