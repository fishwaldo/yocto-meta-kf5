# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/5.108/kdesu-5.108.0.tar.xz"
SRC_URI[sha256sum] = "d08b1ccd280def0550596c8d2e3ca35167536fa3826e676bfe290193c37d1ee6"

