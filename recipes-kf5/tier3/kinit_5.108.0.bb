# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/5.108/kinit-5.108.0.tar.xz"
SRC_URI[sha256sum] = "d11992a58ceabbf4d080477c0f869b19a115c970bd7ada62164f06a0817d03c4"

