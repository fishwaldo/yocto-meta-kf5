# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/5.108/kemoticons-5.108.0.tar.xz"
SRC_URI[sha256sum] = "a629c715e09ae1d567baac6ff22327a462f8b3e223945a89d7409e5d932ef85c"

