# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/5.108/kiconthemes-5.108.0.tar.xz"
SRC_URI[sha256sum] = "73eba332c0704e689367979979ae43328617eb42bbdd742835e4853525f91465"

