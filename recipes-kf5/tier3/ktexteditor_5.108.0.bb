# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/5.108/ktexteditor-5.108.0.tar.xz"
SRC_URI[sha256sum] = "2e5691b074f59390e5ce71cacba7e06693a681e4f991c7d4b6defde3134c5f65"

