# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/5.108/kxmlgui-5.108.0.tar.xz"
SRC_URI[sha256sum] = "417527fbc0e71937c803b8218612d03c8c5bfd3ee4b681f1a26a6d6e10fed66c"

