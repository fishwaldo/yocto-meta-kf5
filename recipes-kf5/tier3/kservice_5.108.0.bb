# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/5.108/kservice-5.108.0.tar.xz"
SRC_URI[sha256sum] = "8227a8dff7e780701ccfefebd0366eb521c9bd792e067ce79a60acd407b6ae81"

