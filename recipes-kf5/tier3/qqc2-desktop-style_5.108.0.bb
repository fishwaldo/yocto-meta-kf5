# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/5.108/qqc2-desktop-style-5.108.0.tar.xz"
SRC_URI[sha256sum] = "89ef4b6de9495da6bd5f4f679d75f64cdc1f714f6f0d7948805c7c80c3419bc5"

