# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/5.108/kbookmarks-5.108.0.tar.xz"
SRC_URI[sha256sum] = "6be1a1a86dac181923b4e27284ab0cb76ea5cf7087c86838793ee77aba888794"

