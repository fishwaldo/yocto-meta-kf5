# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/5.108/kcmutils-5.108.0.tar.xz"
SRC_URI[sha256sum] = "1932a93c549ebde323d11bb5485ab4da1605bd61231dc7fb7fd3a19639411afe"

