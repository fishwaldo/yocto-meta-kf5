# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/5.108/kglobalaccel-5.108.0.tar.xz"
SRC_URI[sha256sum] = "7cb854ca927f0896a96968ce5255dcd27ddfa2bbe91643ec51bb43770ddac669"

