# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/5.108/kitemmodels-5.108.0.tar.xz"
SRC_URI[sha256sum] = "3bc9f5670ebef9fb746d0406cb461540a9bf54d6cd8ed34dce72cdde420bad15"

