# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/5.108/attica-5.108.0.tar.xz"
SRC_URI[sha256sum] = "1893ea8dc611cc26d905bf869279c54d5cc592b3d6e7bd78ba55614a4f6bb195"

