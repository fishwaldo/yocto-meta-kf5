# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/5.108/threadweaver-5.108.0.tar.xz"
SRC_URI[sha256sum] = "492cc40fe25683b184b64042cd582c33b8266743163e7762761ebd0717769624"

