# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/5.108/kwindowsystem-5.108.0.tar.xz"
SRC_URI[sha256sum] = "0dcf6cc7851da5316dece3e23e7045225c826e365f7d1a763280e304fc632204"

