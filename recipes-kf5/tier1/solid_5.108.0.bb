# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/5.108/solid-5.108.0.tar.xz"
SRC_URI[sha256sum] = "280bb5ccf035a9e7559a23749646b35b9a3e02018d5d024db5c7849f353b9154"

