# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/5.108/breeze-icons-5.108.0.tar.xz"
SRC_URI[sha256sum] = "9e6507ff60f4ef251b175da0e573a44dda505648745e62b5c10f6d2aca30af9c"

