# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/5.108/kwayland-5.108.0.tar.xz"
SRC_URI[sha256sum] = "034c74d3d5d3ae1c4d01708adc336e6e28525a9b44bc8915040bd71f7f0eb387"

