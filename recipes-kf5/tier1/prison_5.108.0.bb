# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/5.108/prison-5.108.0.tar.xz"
SRC_URI[sha256sum] = "8453d483d89d08edbc178a643ff5ca1bb35c1aaa00e4e2d34fa87879da15c6de"

