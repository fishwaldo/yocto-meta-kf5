# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/5.108/karchive-5.108.0.tar.xz"
SRC_URI[sha256sum] = "9e9049551ec7e21dbf173e27390d5ce919d34bf31f395bebb467bfec348075e5"

