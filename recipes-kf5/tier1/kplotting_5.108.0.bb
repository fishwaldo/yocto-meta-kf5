# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/5.108/kplotting-5.108.0.tar.xz"
SRC_URI[sha256sum] = "97e821d4f98e69fc84ca6d4d41f9f9f35f2c5f271154feb8c62a0b5d7be7d3e6"

