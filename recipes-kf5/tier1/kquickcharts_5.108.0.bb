# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/5.108/kquickcharts-5.108.0.tar.xz"
SRC_URI[sha256sum] = "fce6ba2b1715ae68847d19a8981f0676c4e077413be1fb6f941820a2a87ab5f1"

