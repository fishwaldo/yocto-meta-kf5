# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/5.108/syntax-highlighting-5.108.0.tar.xz"
SRC_URI[sha256sum] = "7ea620e84556d0d80ac569beaac756b74fac071adb3f2e9aeea044b2174031d3"

