# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/5.108/kholidays-5.108.0.tar.xz"
SRC_URI[sha256sum] = "c55fa8fb87ff93c260bf6f183da2a52f4298db35de60226d457b9f512d41e40d"

