# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/5.108/oxygen-icons5-5.108.0.tar.xz"
SRC_URI[sha256sum] = "ad6f86e9bdf96b247a6666962c6dee0348bbd1a68a61cb800121fd14e3633f71"

