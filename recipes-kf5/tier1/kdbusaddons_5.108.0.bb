# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/5.108/kdbusaddons-5.108.0.tar.xz"
SRC_URI[sha256sum] = "393df3c1623d0354bd244904e9287d9340fd3f22424b7e0df6297f309b4d3bea"

