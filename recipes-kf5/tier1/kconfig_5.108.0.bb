# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/5.108/kconfig-5.108.0.tar.xz"
SRC_URI[sha256sum] = "4daeae499981bbc45ceb841d37a33487eea88910077f11ecfc2a8fee8b03033f"

