# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/5.108/kcoreaddons-5.108.0.tar.xz"
SRC_URI[sha256sum] = "c8b3a1abe2fef61b97dff9d58e29fc14cb01ab153c6feddfcd5bb22a632a0e51"

