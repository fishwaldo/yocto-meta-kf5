# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/5.108/kcalendarcore-5.108.0.tar.xz"
SRC_URI[sha256sum] = "db978bfbfd94020c6ea37cbf0af4fbc98211fd6faf733a92399708737e8fb4f3"

