# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/5.108/ki18n-5.108.0.tar.xz"
SRC_URI[sha256sum] = "59ef5eaf705478f498511b696b31db269861129d11987684a34802902c50ff4e"

