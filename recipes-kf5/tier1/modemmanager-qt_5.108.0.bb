# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/5.108/modemmanager-qt-5.108.0.tar.xz"
SRC_URI[sha256sum] = "3f9fcee87d3ad79715cfa2ac5bad6381c0d7e5fb5d21bf7ff2163ada440d7fe6"

