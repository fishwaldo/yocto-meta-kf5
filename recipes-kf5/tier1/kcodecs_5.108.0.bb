# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/5.108/kcodecs-5.108.0.tar.xz"
SRC_URI[sha256sum] = "810cff7606d4b32dca47b861d815cf5f9dae42d2979febeaddc0b1ce6cda6a8b"

