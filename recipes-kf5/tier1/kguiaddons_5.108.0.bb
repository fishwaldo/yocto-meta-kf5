# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/5.108/kguiaddons-5.108.0.tar.xz"
SRC_URI[sha256sum] = "a14804e91a4388c23e4a6981b84fa5b82e0b3a41c5bbd0ce230945bcbcd8ce07"

