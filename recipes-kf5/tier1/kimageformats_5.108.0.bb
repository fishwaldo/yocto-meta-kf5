# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/5.108/kimageformats-5.108.0.tar.xz"
SRC_URI[sha256sum] = "ec2039d88314ebd76df4397e97b1386a4f2704361d0c0a849d48dc4deddcbe1e"

