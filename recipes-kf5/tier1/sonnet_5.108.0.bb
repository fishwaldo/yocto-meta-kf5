# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/5.108/sonnet-5.108.0.tar.xz"
SRC_URI[sha256sum] = "e11ea6ca8259313b7cb1db292291e2a1e3c13a9e6323d19e2adc81bde5f35f01"

