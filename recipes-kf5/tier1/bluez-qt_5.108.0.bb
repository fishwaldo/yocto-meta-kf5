# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/5.108/bluez-qt-5.108.0.tar.xz"
SRC_URI[sha256sum] = "1ac273d6e3f0d8bda241dcc852fa6cff1a21b7afa8d9e5511827a574f1cac2f9"

