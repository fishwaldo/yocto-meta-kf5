# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/5.108/kidletime-5.108.0.tar.xz"
SRC_URI[sha256sum] = "a77fe78f343fd865b2a19a2c5084fc2961ab8a92603031c6010576ecfb180b33"

