# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/5.108/networkmanager-qt-5.108.0.tar.xz"
SRC_URI[sha256sum] = "4eee430c2033aa70edb59cb5837d2dcf9f60b06f072f717399bf3106890d3079"

