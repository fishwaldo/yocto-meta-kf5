# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/5.108/kitemviews-5.108.0.tar.xz"
SRC_URI[sha256sum] = "6177eeaf6bf92da89511b18a4b720ce1454fed9a9f49d75ed0dc6c4a00f2ac8d"

