# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/5.108/kwidgetsaddons-5.108.0.tar.xz"
SRC_URI[sha256sum] = "e8a009a23a2e6d9ae70be93b185ee641b8e5191aac35896fe9bf15065adffaa8"

